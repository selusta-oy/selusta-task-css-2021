const App = () => {
  return (
    <div className='min-h-screen min-w-screen flex gap-8 justify-center items-center text-4xl'>

      {/* Example with a small amount of li elements */}
      <div
        className=' \
        w-auto h-80 p-4 bg-blue-200 overflow-y-auto overflow-x-hidden \
        flex flex-col gap-8 \
        justify-center items-center'
      >
        <ul className='flex flex-col gap-3'>
          <li>👌</li>
          <li>😁</li>
        </ul>
        <ul className='flex flex-col gap-3'>
          <li>🤷‍♀️</li>
          <li>🎁</li>
        </ul>
      </div>

      {/*
        * The problem example
        * Why 🔥 is first what you see, then clearly it shut be 👌?
        */}
      <div
        className=' \
        w-auto h-80 p-4 bg-blue-200 overflow-y-auto overflow-x-hidden \
        flex flex-col gap-8 \
        justify-center items-center'
      >
        <ul className='flex flex-col gap-3'>
          <li>👌</li>
          <li>😁</li>
          <li>🐿</li>
          <li>🔥</li>
          <li>🌊</li>
          <li>🧡</li>
        </ul>
        <ul className='flex flex-col gap-3'>
          <li>🤷‍♀️</li>
          <li>🎁</li>
          <li>🚍</li>
          <li>😜</li>
          <li>✨</li>
          <li>☔</li>
        </ul>
      </div>

      {/*
        * You answer, and working example
        * There are couple limits:
        * - you can only REMOVE classes from last line of parent div (marked)
        * - you can edit everything inside div
        * - only use Tailwind. if you need CSS
        */}
      <div
        className=' \
        w-auto h-80 p-4 bg-blue-200 overflow-y-auto overflow-x-hidden \
        flex flex-col gap-8 \
        justify-center items-center' /* Remove here, if needed */
      >
        {/* You can start editing here */}
        <ul className='flex flex-col gap-3'>
          <li>👌</li>
          <li>😁</li>
          <li>🐿</li>
          <li>🔥</li>
          <li>🌊</li>
          <li>🧡</li>
        </ul>
        <ul className='flex flex-col gap-3'>
          <li>🤷‍♀️</li>
          <li>🎁</li>
          <li>🚍</li>
          <li>😜</li>
          <li>✨</li>
          <li>☔</li>
        </ul>
        {/* ... and stop here. */}
        {/*
          * REMEBER: Test your answer with a small amount of li elements
          */}
      </div>

    </div>
  )
}

export default App
