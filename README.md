# Selusta task CSS 2021

This is CSS task, for front-end developer applicants 2021.

Fork/copy this repository, and make it private!
Add me ([@JuhRa](https://gitlab.com/JuhRa)) to repository so I can see the answer.

Only edit src/App.js file.

## The task

When you start project, you will see this.
![Starting point](screenshots/startingpoint.png)

This is our goal, see all the emojis.
![](screenshots/goal.png)

Remember check, that you answer also works, with couple of emojis.
![](screenshots/goal-remember.png)

## Available Scripts

In the project directory, you can run:

### `yarn`

Download all modules.

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

## Learn More About Tailwind

[Tailwind CSS](https://tailwindcss.com/) A utility-first CSS framework packed with classes like flex, pt-4, text-center and rotate-90 that can be composed to build any design, directly in your markup.
